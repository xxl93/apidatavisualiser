# README #

### How do I get set up? ###

Clone and install gradle packages.

**Architecture**
MVVM

**Settings search intent**

Location manifest, MainActivity, MainActivityViewModel

android:lunchMode= "singleTop" 

search hints can be placed in @xml/searchable

catching voice search intent result in mainActivity

    :::java
    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            mCardAdapter.filterByName(query);
        }
    }


java setting voice search manager in VM.
    
    :::java
    @BindingAdapter("enableSearchManager")
    static public void enableSearchManager(SearchView searchView, boolean b){
        SearchManager searchManager = (SearchManager) MyApp.mContext.getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(MyApp.mContext, MainActivity.class)));
    }

filter by name in adapter class, can be replaced with filter interface
to remove adapter from that reposibility. But it creates a little more code
    
    :::java
	public void filterByName(String query){ 
        List<Card> filteredList = new ArrayList<>();
        query = query.replaceAll("\\s+","").toLowerCase();
        for(Card c : originalData) { // due to lack of lambda in api<24
            if(colorNameContains(c, query) || rankNameContains(c, query) || codeContains(c, query)){
                filteredList.add(c);
            }
        }
        displayList = filteredList;
        notifyDataSetChanged();
    }

**Setting Master-Detail flow**

Going through activities with passed arguments

    :::java
	public void onItemClick(View view) {
        view.getContext().startActivity(CardDetail.launchDetail(view.getContext(), mCard, isFavorite));
    }


Create bundle 

    :::java
    public static Intent launchDetail(Context context, Card card, boolean isFavorite) {
        Intent intent = new Intent(context, CardDetail.class);
        intent.putExtra("CARD", card);
        intent.putExtra("FAV", isFavorite);
        return intent;
    }


Recive bundle
    
    :::java
	public void initDataBinding(){
		activityCardDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_card_detail);
		Card card = getIntent().getExtras().getParcelable("CARD");
		boolean isFavorite = getIntent().getExtras().getBoolean("FAV");
		cardDetailViewModel = new CardDetailViewModel(card, isFavorite);
		activityCardDetailBinding.setViewModel(cardDetailViewModel);
	}

**Setting Api service**

Can be created using clean retrofit async calls 
and probably be much less code.

Endpoint: https://deckofcardsapi.com/

Interface with calls can be found in DeckOfCardsService

Calls are used in MainActivityViewModel

DataManager is injected to MyApplication 

**TODO**

Tests!!!
Review code

