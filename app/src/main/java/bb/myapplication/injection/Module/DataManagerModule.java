package bb.myapplication.injection.Module;

import bb.myapplication.data.API;
import bb.myapplication.data.DeckOfCardsService;
import bb.myapplication.injection.Scope.PerDataManager;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@Module
public class DataManagerModule {

    public DataManagerModule() {

    }

    @Provides
    @PerDataManager
    DeckOfCardsService provideDeckOfCardsService() {
        return new API().getClient();
    }

    @Provides
    @PerDataManager
    Scheduler provideSubscribeScheduler() {
        return Schedulers.io();
    }
}
