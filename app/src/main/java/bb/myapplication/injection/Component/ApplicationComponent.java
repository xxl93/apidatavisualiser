package bb.myapplication.injection.Component;

import android.app.Application;

import javax.inject.Singleton;

import bb.myapplication.data.DataManager;
import bb.myapplication.injection.Module.ApplicationModule;
import bb.myapplication.view.MainActivity;
import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);

    Application application();
    DataManager dataManager();
}
