package bb.myapplication.injection.Component;

import bb.myapplication.data.DataManager;
import bb.myapplication.injection.Module.DataManagerModule;
import bb.myapplication.injection.Scope.PerDataManager;
import dagger.Component;

@PerDataManager
@Component(dependencies = ApplicationComponent.class, modules = DataManagerModule.class)
public interface DataManagerComponent{

    void inject (DataManager dataManager);
}
