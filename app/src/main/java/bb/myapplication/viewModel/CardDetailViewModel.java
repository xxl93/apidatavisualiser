package bb.myapplication.viewModel;


import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import bb.myapplication.R;
import bb.myapplication.model.Card;

public class CardDetailViewModel {

    private Card mCard;
    public boolean favorite;

    public CardDetailViewModel(Card card, Boolean fav){
        mCard = card;
        favorite = fav;
    }

    public String getCode(){ return mCard.getCardRank() + " " + mCard.getCardColor(); }

    public String getPicture(){
        return mCard.imageUrl;
    }

    @BindingAdapter("imageUrl")
    public static void getImg(final ImageView imageView, String url){

        RequestOptions options = new RequestOptions().placeholder(R.drawable.ic_suggestion);

        Glide.with(imageView.getContext())
                .load(url)
                .apply(options)
                .into(imageView);
    }
}
