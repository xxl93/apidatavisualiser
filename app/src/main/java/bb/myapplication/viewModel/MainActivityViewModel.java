package bb.myapplication.viewModel;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import java.lang.reflect.Field;

import bb.myapplication.MyApp;
import bb.myapplication.data.DataManager;
import bb.myapplication.model.CardList;
import bb.myapplication.view.MainActivity;
import bb.myapplication.view.adapter.CardAdapter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

public class MainActivityViewModel extends BaseObservable {

    private DataManager mDataManager;
    private static CardAdapter mCardAdapter;
    private CompositeDisposable mCompositeDisposable;
    final private int numberOfCards = 30;
    final private int numberOfDecks = 5;
    public ObservableField<Boolean> progressBarVisible = new ObservableField<>();
    public boolean b = true;

    public MainActivityViewModel(CardAdapter cardAdapter) {
        mCompositeDisposable = new CompositeDisposable();
        mDataManager = MyApp.get().getComponent().dataManager();
        mCardAdapter = cardAdapter;
        getCardList("new", numberOfCards);
    }

    void getCardList(String deckId, Integer numberOfCards){
        progressBarVisible.set(true);
        mCompositeDisposable.add(mDataManager.getCards(deckId,numberOfCards, numberOfDecks)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(mDataManager.getScheduler())
            .subscribeWith(new DisposableObserver<CardList>() {
                @Override
                public void onNext(@NonNull CardList cardList) {
                    mCardAdapter.setCardList(cardList.cardList); //TODO Check if there is a better way
                    notifyChange();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Toast.makeText(MyApp.get(), "There was a problem to get cards", Toast.LENGTH_SHORT).show();
                    mCompositeDisposable.clear();
                }

                @Override
                public void onComplete() {
                    progressBarVisible.set(false);
                }
            })
        );
    }

    @BindingAdapter("enableSearchManager")
    static public void enableSearchManager(SearchView searchView, boolean b){
        SearchManager searchManager = (SearchManager) MyApp.mContext.getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(MyApp.mContext, MainActivity.class)));
    }

    @BindingAdapter("enableQueryListener")
    static public void enableQueryListener(SearchView searchView, boolean b){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mCardAdapter.filterByName(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mCardAdapter.filterByName(newText);
                return false;
            }
        });
    }

    @BindingAdapter("swipe")
    static public void enableSwipe(SwipeRefreshLayout swipeRefreshLayout, SwipeRefreshLayout.OnRefreshListener ls){
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(ls);
    }

    public SwipeRefreshLayout.OnRefreshListener listener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getCardList("new", 30);
        }
    };

}
