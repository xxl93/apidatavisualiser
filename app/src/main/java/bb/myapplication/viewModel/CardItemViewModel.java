package bb.myapplication.viewModel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import bb.myapplication.R;
import bb.myapplication.model.Card;
import bb.myapplication.view.CardDetail;

public class CardItemViewModel extends BaseObservable {

    private Card mCard;
    public boolean isFavorite = false;

    public CardItemViewModel(Card card){
        mCard = card;
    }

    public String getCode(){ return mCard.getCardRank() + " " + mCard.getCardColor(); }

    public String getPicture(){
        return mCard.imageUrl;
    }

    public Card getCard(){
        return mCard;
    }

    @BindingAdapter("imageUrl")
    public static void getImg(final ImageView imageView, String url){

        RequestOptions options = new RequestOptions().placeholder(R.drawable.ic_suggestion);

        Glide.with(imageView.getContext())
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public void setCard(Card card) {
        mCard = card;
        notifyChange();
    }

    public void onItemClick(View view) {
        view.getContext().startActivity(CardDetail.launchDetail(view.getContext(), mCard, isFavorite));
    }

    @BindingAdapter("onChangeListener")
    static public void onChange(final ToggleButton toggleButton, boolean b){

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }
}
