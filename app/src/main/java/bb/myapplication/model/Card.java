package bb.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Card implements Parcelable{

    @SerializedName("suit")
    public String suit;
    @SerializedName("images")
    public Images img;
    @SerializedName("image")
    public String imageUrl;
    @SerializedName("code")
    public String code;
    @SerializedName("value")
    public String value;

    protected Card(Parcel in) {

        code = in.readString();
        imageUrl = in.readString();
        suit = in.readString();
        value = in.readString();
    }

    public String getCardRank(){
        switch (code.charAt(0)){
            case 'A': return "Ace";
            case '0': return "10";
            case 'J': return "Jack";
            case 'Q': return "Queen";
            case 'K': return "King";
            default:
                return String.valueOf(code.charAt(0));
        }
    }

    public String getCardColor(){
        switch (code.charAt(1)){
            case 'H': return "Heart";
            case 'S': return "Spike";
            case 'C': return "Clubs";
            case 'D': return "Diamond";
            default: throw new IllegalArgumentException();
        }
    }

    public Card(String code) {
        this.code = code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(imageUrl);
        dest.writeString(suit);
    }

    public static final Parcelable.Creator<Card> CREATOR
            = new Parcelable.Creator<Card>() {
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        public Card[] newArray(int size) {
            return new Card[size];
        }
    };
}
