package bb.myapplication.model;

public class Deck {
    public String deck_id;
    public Boolean success;
    public Integer remaining;
    public Boolean shuffled;
}
