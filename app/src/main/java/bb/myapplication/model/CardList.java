package bb.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CardList{

    @SerializedName("deck_id")
    public String deck_id;

    @SerializedName("cards")
    public List<Card> cardList = new ArrayList<>();

    @SerializedName("success")
    public Boolean success;

    @SerializedName("remaining")
    public Integer remaining = 0;

    public String getDeck_id() {
        return deck_id;
    }
}
