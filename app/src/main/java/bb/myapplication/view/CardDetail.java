package bb.myapplication.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bb.myapplication.R;
import bb.myapplication.model.Card;
import bb.myapplication.viewModel.CardDetailViewModel;
import bb.myapplication.databinding.ActivityCardDetailBinding;

public class CardDetail extends AppCompatActivity {

    ActivityCardDetailBinding activityCardDetailBinding;
    CardDetailViewModel cardDetailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);
        initDataBinding();
    }

    public void initDataBinding(){
        activityCardDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_card_detail);
        Card card = getIntent().getExtras().getParcelable("CARD");
        boolean isFavorite = getIntent().getExtras().getBoolean("FAV");
        cardDetailViewModel = new CardDetailViewModel(card, isFavorite);
        activityCardDetailBinding.setViewModel(cardDetailViewModel);
    }
    public static Intent launchDetail(Context context, Card card, boolean isFavorite) {
        Intent intent = new Intent(context, CardDetail.class);
        intent.putExtra("CARD", card);
        intent.putExtra("FAV", isFavorite);
        return intent;
    }
}
