package bb.myapplication.view.adapter;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bb.myapplication.R;
import bb.myapplication.databinding.CardRowBinding;
import bb.myapplication.model.Card;
import bb.myapplication.viewModel.CardItemViewModel;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder>{
    private List<Card> displayList = new ArrayList<>(); //Can be changed to SortedList with butch updates.
    private List<Card> originalData = new ArrayList<>();

    public void setCardList(List<Card> cardList) {
        displayList = cardList;
        originalData = displayList;
        notifyDataSetChanged();
    }

    public int filterByName(String query){ //Can be replaced with filter interface. But idk why would I.
        List<Card> filteredList = new ArrayList<>();
        query = query.replaceAll("\\s+","").toLowerCase();
        for(Card c : originalData) { // due to lack of lambda in api<24
            if(colorNameContains(c, query) || rankNameContains(c, query) || codeContains(c, query)){
                filteredList.add(c);
            }
        }
        displayList = filteredList;
        notifyDataSetChanged();
        return displayList.size();
    }
    
    boolean colorNameContains(Card c, String query){
        return c.getCardColor().toLowerCase().contains(query);
    }
    
    boolean rankNameContains(Card c, String query){
        return c.getCardRank().toLowerCase().contains(query);
    }
    
    boolean codeContains(Card c, String query) {
        return (c.code.toLowerCase().contains(query));
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardRowBinding cardRowBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.card_row,
                        parent, false);
        return new CardViewHolder(cardRowBinding);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        holder.bindCard(displayList.get(position));
    }

    @Override
    public int getItemCount() {
        return displayList.size();
    }


    class CardViewHolder extends RecyclerView.ViewHolder {
        CardRowBinding mCardRowBinding;

        public CardViewHolder(CardRowBinding cardRowBinding) {
            super(cardRowBinding.cardRow);
            this.mCardRowBinding = cardRowBinding;
        }

        void bindCard(Card card) {
            if (mCardRowBinding.getViewModel() == null) {
                mCardRowBinding.setViewModel(
                        new CardItemViewModel(card));
            } else {
                mCardRowBinding.getViewModel().setCard(card);
            }
        }
    }
}
