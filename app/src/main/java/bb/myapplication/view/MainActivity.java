package bb.myapplication.view;

import android.app.SearchManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import bb.myapplication.R;
import bb.myapplication.databinding.ActivityMainBinding;
import bb.myapplication.view.adapter.CardAdapter;
import bb.myapplication.viewModel.MainActivityViewModel;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding mainActivityBinding;
    MainActivityViewModel mainActivityViewModel;
    public CardAdapter mCardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
    }

    public void initDataBinding(){
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupAdapter();
        mainActivityViewModel = new MainActivityViewModel(mCardAdapter);
        mainActivityBinding.setViewModel(mainActivityViewModel);
    }

    private void setupAdapter(){
        mCardAdapter= new CardAdapter();
        mainActivityBinding.list.setAdapter(mCardAdapter);
        mainActivityBinding.list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            mCardAdapter.filterByName(query);
        }
    }
}
