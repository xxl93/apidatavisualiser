package bb.myapplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;
import org.robolectric.RobolectricTestRunner;

import bb.myapplication.model.Card;
import bb.myapplication.viewModel.CardDetailViewModel;

import static junit.framework.Assert.assertEquals;


@RunWith(RobolectricTestRunner.class) @Config(constants = BuildConfig.class, sdk = 21)
public class CardDetailViewModelTest {

    private CardDetailViewModel cardDetailViewModel;
    private Card card;

    @Before public void setUpDetailViewModelTest(){
        card = new Card("JD");
        cardDetailViewModel = new CardDetailViewModel(card, true);
    }

    @Test public void shouldGetCardCode(){
        assertEquals(card.getCardRank() + " " +  card.getCardColor(), cardDetailViewModel.getCode());
    }

    @Test public void shouldReturnEmptyImageUrl(){
        assertEquals(card.imageUrl, cardDetailViewModel.getPicture());
    }

    @Test public void favIconShouldBeChecked(){
        assertEquals(true, cardDetailViewModel.favorite);
    }
}
