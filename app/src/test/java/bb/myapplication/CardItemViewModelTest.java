package bb.myapplication;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ToggleButton;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import bb.myapplication.databinding.CardRowBinding;
import bb.myapplication.model.Card;
import bb.myapplication.view.CardDetail;
import bb.myapplication.view.MainActivity;
import bb.myapplication.viewModel.CardItemViewModel;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class) @Config(constants = BuildConfig.class, sdk = 21)
public class CardItemViewModelTest {
    String image_url = "http://deckofcardsapi.com/static/img/9S.png";
    String code = "JD";

    private CardItemViewModel cardItemViewModel;
    private CardRowBinding cardRowBinding;
    private MyApp myApp;
    Card card;

    @Before public void setUpCardItemViewModelTest(){
        myApp = (MyApp) RuntimeEnvironment.application;
        card = new Card(code);
        card.imageUrl = image_url;
        cardItemViewModel = new CardItemViewModel(card);
    }

    @Test
    public void shouldGetCardRow(){
        assertEquals(card, cardItemViewModel.getCard());
    }

    @Test public void shouldStartCardDetailActivityOnItemClick(){
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        cardItemViewModel.onItemClick(new View(MyApp.mContext));
        Intent expectedIntent = new Intent(MyApp.mContext, CardDetail.class);
        assertTrue(shadowOf(activity).getNextStartedActivity().filterEquals(expectedIntent));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Test public void shouldCheckFaviconOnClick(){
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        activity.mCardAdapter.setCardList(new ArrayList<Card>(){{add(card);}});


        ToggleButton tb = (ToggleButton) activity.findViewById(R.id.imageButton2);
        tb.performClick();
        shadowOf(tb).checkedPerformClick();
        assertTrue(tb.isChecked());
    }



}
