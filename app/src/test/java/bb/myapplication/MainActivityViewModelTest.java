package bb.myapplication;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.widget.SearchView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import bb.myapplication.databinding.ActivityMainBinding;
import bb.myapplication.data.DeckOfCardsService;
import bb.myapplication.model.Card;
import bb.myapplication.view.MainActivity;
import bb.myapplication.view.adapter.CardAdapter;
import bb.myapplication.viewModel.MainActivityViewModel;
import io.reactivex.Observable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class) @Config(constants = BuildConfig.class, sdk = 21)
public class MainActivityViewModelTest {

    private MainActivityViewModel mainActivityViewModel;
    private MyApp app;
    @Mock private DeckOfCardsService deckOfCardsService;
    @Mock private ActivityMainBinding activityMainBinding;

    @Before public void setMainActivityViewModelTest(){
        app  = (MyApp) RuntimeEnvironment.application;
        MockitoAnnotations.initMocks(this);

        CardAdapter cardAdapter = new CardAdapter();
        cardAdapter.setCardList( MockApi.getCardList());
        mainActivityViewModel = new MainActivityViewModel(cardAdapter);
    }

    @Test
    public void simulateGivenTheUserCallListFromApi() throws Exception {
        List<Card> cards = MockApi.getCardList();
        doReturn(Observable.just(cards)).when(deckOfCardsService).getCards("new",2, 2);
    }

    @Test
    public void shouldEnableSearchManager(){
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);

        SearchManager searchManager = (SearchManager) mainActivity.getSystemService(Context.SEARCH_SERVICE);
        assertTrue(searchManager != null);
    }

    @Test
    public void shouldEnableQueryListener(){
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        SearchView searchView = (SearchView) mainActivity.findViewById(R.id.search);


        //TODO
    }

    @Test public void shouldReturnFilteredQuery(){
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);

        mainActivity.mCardAdapter.setCardList(MockApi.getCardList());
        assertEquals(mainActivity.mCardAdapter.filterByName("s"),0);
        assertEquals(mainActivity.mCardAdapter.filterByName("d"),3);

    }
}
