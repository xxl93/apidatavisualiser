package bb.myapplication;

import java.util.ArrayList;
import java.util.List;

import bb.myapplication.model.Card;

public class MockApi {

    private static final String code = "JD";
    private static final String url = "http://deckofcardsapi.com/static/img/JD.png";


    public static List<Card> getCardList(){
        List<Card> list = new ArrayList<>();
        for(int i =0; i < 3; i ++){
            list.add(getCard());
        }
        return list;
    }

    public static Card getCard(){
        Card c = new Card(code);
        c.imageUrl = url;
        return c;
    }
}
